#File System Storage and PYMKEWS configuration
import pymkews_controller
import os

controller_config = {
                        'data_storage': {
                            'type': 'osfs',
                            'params': {
                                'root': ''
                            }
                        },
                        'job_executor': {
                            'type': 'testing',
                            'params': {
                                'execution_rate': 0.2,
                                'failure_rate': 0.5
                            }
                        }
                    }

# DataStorage


data_storage_root = ('/'.join((os.path.abspath(__file__)).split('/')[:-2])) + '/filesystem'
controller_config['data_storage']['params']['root'] = data_storage_root
print(f"Created data storage root: {data_storage_root}")

data_storage = pymkews_controller.create_data_storage(controller_config['data_storage'])
job_executor = pymkews_controller.create_job_executor(controller_config['job_executor'], data_storage)