#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
    MagikEye APi's For Handling Datasets and Jobs in DataBase
"""
__author__ = "karthik"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."
# ----------------------------------------------------------------------------------------------------------------------

from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from django.http import JsonResponse
from django.http import FileResponse
from dbapi.models import Datasetsdata, Jobsdata

import json, base64
from dbapi import pymkews_conf as PYMKEWS
# API's communicate with DB:

@api_view(["POST"])
def createDataSet(payload):
    """
    :Author:  karthik
    :Description: API to create a dataset for a given user
    :param payload: Dict ['username': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        ds = {
                    'username': data["username"],
                    'comment': "None, Can be updated with the chat icon here!"
                   }

        current_data_set = Datasetsdata.objects.create(dataset_user=ds['username'], dataset_date_comment=ds['comment'])
        current_data_set.save()

        data_set_id = Datasetsdata.objects.filter(dataset_user=ds['username']).last().id
        data.update({'data_set_id': data_set_id, 'type': 'success'})

        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def updateDataSet(payload):
    """
    :Author:  karthik
    :Description: API to update dataset's comment for a given user
    :param payload: Dict ['data_set_id': str, 'comment': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        dataset_id = data["data_set_id"]
        comment = data["comment"]

        current_data_set = Datasetsdata.objects.filter(id=dataset_id)
        current_data_set.update(dataset_date_comment=comment)

        data.update({'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def removeDataSet(payload):
    """
    :Author:  karthik
    :Description: API to remove a dataset for a given user
    :param payload: Dict ['data_set_id': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        dataset_id = data["data_set_id"]

        current_data_set = Datasetsdata.objects.filter(id=dataset_id)
        current_data_set.delete()

        data.update({'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeAlldatasets(payload):
    """
    :Author:  karthik
    :Description: API to remove all datasets for a given user, Will be called on user deletion.
    :param payload: Dict ['username': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        username = data["username"]

        all_data_sets_of_user = Datasetsdata.objects.filter(dataset_user=username)
        all_data_sets_of_user.delete()

        data.update({'message': f'All Data sets of {username} has been deleted successfully', 'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def createJob(payload):
    """
    :Author:  karthik
    :Description: API to create a job for a given user
    :param payload: Dict ['username': str, 'dataset_id': str, 'jobs_type': str, 'jobs_process': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        job = {
                "username": data["username"],
                "dataset_id": data["dataset_id"],
                "type": data["jobs_type"],
                "process": data["jobs_process"]
              }

        current_job = Jobsdata.objects.create(jobs_user=job["username"], jobs_type=job["type"],
                                              jobs_process=job["process"], jobs_dataset_id=job["dataset_id"])
        current_job.save()

        job_id = Jobsdata.objects.filter(jobs_user=job['username']).last().id
        data.update({'job_id': job_id, 'type': 'success'})

        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def updateJob(payload):
    """
    :Author:  karthik
    :Description: API to update jobs's process status comment for a given job id
    :param payload: Dict ['job_id': str, 'jobs_process': str]
    :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        job = {
                "job_id": data["job_id"],
                "process": data["jobs_process"]
              }

        current_job = Jobsdata.objects.filter(id=job["job_id"])
        current_job.update(jobs_process=job["process"])

        data.update({'type': 'Success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def removeJob(payload):
    """
        :Author:  karthik
        :Description: API to remove a job for a given job id
        :param payload: Dict ['job_id': str]
        :return: JsonResponse
        """
    try:
        data = json.loads(payload.body)
        job_id = data["job_id"]
        current_job = Jobsdata.objects.filter(id=job_id)
        current_job.delete()

        data.update({'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeAlljobs(payload):
    """
        :Author:  karthik
        :Description: API to remove all jobs for a given user, Will be called on user deletion.
        :param payload: Dict ['username': str]
        :return: JsonResponse
    """
    try:
        data = json.loads(payload.body)
        username = data["username"]

        all_jobs_of_user = Jobsdata.objects.filter(jobs_user=username)
        all_jobs_of_user.delete()

        data.update({'message': f'All Jobs of {username} has been deleted successfully', 'type': 'success'})
        return JsonResponse(data, safe=False)

    except Exception as msg:
        return Response(str(msg), status.HTTP_400_BAD_REQUEST)



# API's communicate with filesystem:

@api_view(["GET"])
def getFSUsers(payload):
    try:
        users = PYMKEWS.data_storage.list_users()
        users = [users] if not isinstance(users, list) else users
        response = {"users": users}
        return Response(response, status.HTTP_200_OK)
    except Exception as err_response:
        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def createFSUser(payload):
    try:
        data = json.loads(payload.body)
        username = data["username"]
        if username not in PYMKEWS.data_storage.list_users():
            PYMKEWS.data_storage.create_user(username)
            response = {"message": f"User '{username}' has been added in the filesystem", "type": "success"}
        else:
            response = {"message": f"User '{username}' has been already added in the filesystem", "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if 'username' not in data:
            err_response = {"message": "Please provide the Field 'username' in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def createFSDataset(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        data_set_id = data["data_set_id"]
        if int(data_set_id) not in PYMKEWS.data_storage.list_datasets(username):
            PYMKEWS.data_storage.create_dataset(username, data_set_id)
            response = {"message": f"Dataset '{data_set_id}' has been created for the user '{username}'", "type": "success"}
        else:
            response = {"message": f"Dataset '{data_set_id}' has been already created for the user '{username}'.",
                        "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide mandatory Fields 'username' and 'data_set_id' in payload",
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFSDatasets(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        fs_data_set_ids = PYMKEWS.data_storage.list_datasets(username)
        fs_data_set_ids = [fs_data_set_ids] if not isinstance(fs_data_set_ids, list) else fs_data_set_ids
        datasets = dict()
        for data_set_id in fs_data_set_ids:
            dataset_url = PYMKEWS.data_storage.get_dataset_url(username, data_set_id)
            dataset_files = PYMKEWS.data_storage.list_dataset_files(username, data_set_id)
            datasets[str(data_set_id)] = (dataset_url, dataset_files)

        response = {"datasets": datasets, "type": "success"}
        return Response(response, status.HTTP_200_OK)
    except Exception as err_response:
        if 'username' not in data:
            err_response = {"message": "Please provide the Field 'username' in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def addFileToFSDataset(payload):
    data = payload.data
    try:
        username = data["username"]
        data_set_id = data["data_set_id"]
        file_name = data["filename"]
        file_content = data["file"]

        with open(f"{file_name}", "wb") as f:
            f.write(file_content.read())
            f.flush()
        PYMKEWS.data_storage.add_file_to_dataset(username, data_set_id, f.name)

        response = {"message": f"File {f.name} added to the data set {data_set_id}", "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("data_set_id" not in data) or ("filename" not in data) or ("file" not in data):
            err_response = {"message": "Please provide mandatory Fields ['username', 'data_set_id', 'filename', 'file']\
                            in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
def listFSDatasetFiles(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        data_set_id = data["data_set_id"]
        data_set_files = PYMKEWS.data_storage.list_dataset_files(username, data_set_id)
        data_set_files = [data_set_files] if not isinstance(data_set_files, list) else data_set_files
        response = {"data_set_files": data_set_files, "type": "success"}
        return Response(response, status.HTTP_200_OK)
    except Exception as err_response:
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide mandatory Fields 'username' and 'data_set_id' in payload",
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFileFromFSDataset(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        data_set_id = data["data_set_id"]
        file_name = data["file_name"]
        file_content = PYMKEWS.data_storage.get_file_from_dataset(username, data_set_id, file_name)
        return FileResponse(file_content, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("data_set_id" not in data) or ("file_name" not in data):
            err_response = {"message": "Please provide mandatory Fields ['username', 'data_set_id', 'file_name']\
                    in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeFSDataset(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        data_set_id = data["data_set_id"]
        if int(data_set_id) in PYMKEWS.data_storage.list_datasets(username):
            PYMKEWS.data_storage.delete_dataset(username, data_set_id)
            response = {"message": f"Dataset '{data_set_id}' has been deleted for the user '{username}'",
                        "type": "success"}
        else:
            response = {"message": f"Dataset '{data_set_id}' has been already deleted for the user '{username}'.",
                        "type": "success"}

        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("data_set_id" not in data):
            err_response = {"message": "Please provide mandatory Fields ['username', 'data_set_id'] in payload",
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def createFSJob(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        job_id = data["job_id"]
        job_file_as_dict = data["job_file_as_dict"]

        # Job File Validation - [Job file content validation]
        job_file_validated_successfully = True
        try:
            job_type, job_json = PYMKEWS.job_executor.validate_job(username, job_json=job_file_as_dict)
        except Exception as msg:
            job_file_validated_successfully = False
            if "dataset url does not exist" in str(msg).lower():
                dataset_url = '/filesystem' + str(msg).split('filesystem')[1]
                msg = "Dataset URL does not exist:  " + dataset_url
            err_message = f"Cannot validate job JSON: {str(msg)}"

        pstatus = PYMKEWS.job_executor.PROCESS_UNKNOWN

        if job_file_validated_successfully:
            # Execute job [#create job in data_storage is taken care by the job_executor]
            pid = PYMKEWS.job_executor.execute_job(username, job_id, job_file_as_dict)

            while pstatus >= 0:
                pstatus, errmsg = PYMKEWS.job_executor.query_process(pid)

            if pstatus == PYMKEWS.job_executor.PROCESS_SUCCESS:
                pstatus = f"Process successful: {pid}"
            elif pstatus == PYMKEWS.job_executor.PROCESS_FAILURE:
                pstatus = f"Process failure: {pid}, {errmsg}"
            else:
                pstatus = f"Process running: {pid}"

            response = {"pstatus": pstatus, "type": "success"}
            return Response(response, status.HTTP_200_OK)

        else:
            err_response = {"validationError": 1, "message": err_message, "type": "error"}
            return Response(err_response, status.HTTP_406_NOT_ACCEPTABLE)

    except Exception as err_response:
        if ("username" not in data) or ("job_id" not in data) or ("job_file_as_dict" not in data):
            err_response = {"message": "Please provide all mandatory fields ['username', 'job_id, 'job_file_as_dict']\
                                  in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFSJobs(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        jobs = dict()
        jobs_list = PYMKEWS.data_storage.list_jobs(username)
        for job_id in jobs_list:
            job_url = PYMKEWS.data_storage.get_job_url(username, job_id)
            jobs[str(job_id)] = job_url

        response = {"jobs": jobs, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if 'username' not in data:
            err_response = {"message": "Please provide the Field 'username' in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def getFileFromFSJob(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        job_id = data["job_id"]
        file_name = data["file_name"]
        job_file_content = PYMKEWS.data_storage.get_file_from_job(username, job_id, file_name)
        return FileResponse(job_file_content, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("job_id" not in data) or ("file_name" not in data):
            err_response = {"message": "Please provide the Fields ['username', 'job_id', 'file_name'] in payload",\
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def removeFSJob(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        job_id = data["job_id"]
        PYMKEWS.data_storage.delete_job(username, job_id)
        response = {"message": f"Job-{job_id} has been removed from FileSystem successfully!", "type": "success"}
        return Response(response, status.HTTP_200_OK)
    except Exception as err_response:
        if ("username" not in data) or ("job_id" not in data):
            err_response = {"message": "Please provide the Fields ['username', 'job_id]' in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def deleteFSUser(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        if username in PYMKEWS.data_storage.list_users():
            PYMKEWS.data_storage.delete_user(username)
            response = {"message": f"User '{username}' has been removed from the filesystem", "type": "success"}
        else:
            response = {"message": f"User '{username}' had been already removed from the filesystem", "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if 'username' not in data:
            err_response = {"message": "Please provide the Field 'username' in payload", "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)

@api_view(["POST"])
def jobExecutorQueryProcess(payload):
    data = json.loads(payload.body)
    try:
        username = data["username"]
        job_id = data["job_id"]
        pid = data["pid"]
        process_status = PYMKEWS.job_executor.query_process(pid)
        response = {"process_status": process_status, "type": "success"}
        return Response(response, status.HTTP_200_OK)

    except Exception as err_response:
        if ("username" not in data) or ("job_id" not in data) or ('pid' not in data):
            err_response = {"message": "Please provide the Fields ['username', 'job_id', 'pid'] in payload",
                            "type": "error"}
        return Response(err_response, status.HTTP_400_BAD_REQUEST)