FROM ubuntu:18.04

# Install dependencies
RUN apt-get update && apt-get install -y \
    python3-pip

# ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt /requirements.txt
RUN python3 -m pip install -r requirements.txt

RUN mkdir /app
WORKDIR /app
COPY . /app

# RUN adduser -D user
# USER user

