# -*- coding: utf-8 -*-

"""
MkEWS Job Executor
"""

__author__ = "Jan Heller"
__copyright__ = "Copyright (c) 2020, Magik-Eye Inc."

# ------------------------------------------------------------------------------

from jsonschema import validate, ValidationError, SchemaError

from .error import Error
from .data_storage import DataStorage

from abc import ABC, abstractmethod
from typing import Optional
from random import random
import logging
import json


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class JobExecutor(ABC):
    PROCESS_UNKNOWN = 0
    PROCESS_RUNNING = 1
    PROCESS_SUCCESS = -1
    PROCESS_FAILURE = -2

    @abstractmethod
    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        pass

    @abstractmethod
    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> int:
        pass

    @abstractmethod
    def query_process(self, pid: int) -> (int, str):
        pass

    @abstractmethod
    def delete_job(self, user_name: str, job_id: int, pid: int) -> None:
        pass


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

class TestingJobExecutor(JobExecutor):

    def __init__(self, config: dict, data_storage: DataStorage) -> None:
        self.config = config
        self.data_storage = data_storage
        self.logger = logging.getLogger('pymkews.job_executor')
        self.processes = {}
        self.processes_id = 0

    # -------------------------------------------------------------------------

    def validate_job(self, user_name: str, job_json_path: str = '', job_json: Optional[dict] = None) -> (str, dict):
        if job_json_path:
            with open(job_json_path) as file:
                job_json = json.load(file)

        job_type = job_json["type"] + "." + job_json["params"]["template"] + \
                   '.' + job_json["version"]

        # Validate dataset_id

        dts_path = self.data_storage.get_dataset_url(user_name, job_json["dataset_id"], exists=True)

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Validated Job type: {job_type}')

        return job_type, job_json

    # -------------------------------------------------------------------------

    def execute_job(self, user_name: str, job_id: int, job_json: dict) -> int:
        # Create new job in the data storage
        job_url = self.data_storage.create_job(user_name, job_id)
        dts_url = self.data_storage.get_dataset_url(user_name, job_json['dataset_id'])

        process_info = {
            'job_url': job_url,
            'dts_url': dts_url,
            'job_id': job_id,
            'job': job_json
        }

        self.processes_id += 1
        pid = self.processes_id
        self.processes[pid] = process_info

        self.data_storage.add_data_to_job(user_name, job_id, 'job.json', json.dumps(job_json, indent=2).encode('utf-8'))

        if self.logger.isEnabledFor(logging.INFO):
            self.logger.info(f'Executed process, job_id:{job_id}, pid:{pid}')

        return pid

    # -------------------------------------------------------------------------

    def query_process(self, pid: int) -> (int, str):
        if pid not in self.processes:
            return self.PROCESS_UNKNOWN

        t = self.config['execution_rate']
        fr = self.config['failure_rate']

        if random() < t:
            self.processes.pop(pid)

            if random() < fr:
                return self.PROCESS_FAILURE, 'Process error message'
            return self.PROCESS_SUCCESS, ''

        else:
            return self.PROCESS_RUNNING, ''

    # -------------------------------------------------------------------------

    def delete_job(self, user_name: str, job_id: int, pid: int) -> None:
        # Kill process
        if pid in self.processes:
            p = self.processes[pid]
            self.processes.pop(pid)

            if self.logger.isEnabledFor(logging.INFO):
                job_id = p['job_id']
                self.logger.info(f'Killed process: job_id:{job_id}, pid:{pid}')
        else:
            if self.logger.isEnabledFor(logging.INFO):
                self.logger.info(f'Cannot kill process, does not exist: {pid}')

        # Delete job
        self.data_storage.delete_job(user_name, job_id)


# ------------------------------------------------------------------------------
# ------------------------------------------------------------------------------

def create_job_executor(config, data_storage: DataStorage):
    if 'type' not in config:
        raise Error('Unknown JobExecutor type, please specify "type" property in the configuration')

    if config['type'] == 'testing':
        return TestingJobExecutor(config['params'], data_storage)
    else:
        raise Error(f'Unknown JobExecutor type: {config["type"]}')